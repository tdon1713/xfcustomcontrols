# Description
Enhanced layout for an Entry control with the following capabilities:
* Turn off Keyboard on Focus
* Keyboard toggle button
* Select All on Entry
* Inline Dropdown
* Tree View

# How To Include In Your Project
1. To be written

# Usage

# Functionality
## Entry Layout
### Basic XAML Usage
```
<ContentPage xmlns="http://xamarin.com/schemas/2014/forms"
             xmlns:x="http://schemas.microsoft.com/winfx/2009/xaml"
             xmlns:controls="clr-namespace:XFControls.Controls"
             x:Class="XFControls.MainPage">
    <ContentPage.Resources>
        <ResourceDictionary>
            <Style x:Key="EntryLayoutEnhancedEntryStyle" TargetType="controls:EnhancedEntry">
                <Setter Property="SelectAllOnFocus" Value="False"/>
                <Setter Property="ShowKeyboardOnFocus" Value="True"/>
            </Style>

            <Style x:Key="EntryLayoutStyle" TargetType="controls:EntryLayout">
                <Setter Property="EntryStyle" Value="{StaticResource EntryLayoutEnhancedEntryStyle}"/>
            </Style>
        </ResourceDictionary>
    </ContentPage.Resources>
    
    <ContentPage.Content>
        <StackLayout>
            <controls:EntryLayout HeaderText="Option:" EntryText="{Binding SelectedDropdownItem.Value}" 
                                  Style="{StaticResource EntryLayoutStyle}" HeaderPosition="Placeholder"
                                  ShowKeyboardButton="True" ItemsSource="{Binding DropdownItems}"
                                  DropdownSelectedItem="{Binding SelectedDropdownItem}"
                                  ShowFrameBorder="False" ShowFrameShadow="True" AllowManualEntry="False">
                <controls:EntryLayout.LayoutContent>
                    <Label Text="Internal Content" />
                </controls:EntryLayout.LayoutContent>
            </controls:EntryLayout>
        </StackLayout>
    </ContentPage.Content>
</ContentPage>
```

| Property | Type | Default |Description |
|---|---|---|---|
|**ShowFrameShadow**|Boolean|False|Whether a shadow should be wrapped around the frame container of the layout|
|**ShowFrameBorder**|Boolean|False|Whether a border should be shown on the frame container of the layout|

### _Header_
| Property | Type | Default |Description |
|---|---|---|---|
|**HeaderText**|String|Empty String|The text to display above the _**Enhanced Entry**_ control|
|**HeaderPosition**|EntryLayoutHeaderPosition|Top||

### _Entry_
| Property | Type | Default |Description |
|---|---|---|---|
|**EntryStyle**|Style|null|The style to be used if wanting to modify the styles of _**Enhanced Entry**_ or _**Enhanced Button**_|
|**EntryText**|String|Empty String|The value of the _**Enhanced Entry**_ control|
|**EntryCompletedCommand**|ICommand|null|The Command to execute when entry of data into the _**Enhanced Entry**_ is completed is executed|
|**SearchCommand**|ICommand|null|The Command to execute if an external search capability is to be implemented|
|**ShowKeyboardButton**|Boolean|False|Whether the keyboard toggle button should be shown|

### _Dropdown_
| Property | Type | Default |Description |
|---|---|---|---|
|**ItemsSource**|System.Collections.IEnumerable|null|The datasource for the dropdown|
|**DropdownSelectedItem**|object|null|The selected item in the datasource|
|**AllowManualEntry**|Boolean|true|Determines whether the user is allowed to enter a manual item when _ItemsSource_ is not null|

### _Internal View_
| Property | Type | Default |Description |
|---|---|---|---|
|**LayoutContent**|View|null|The custom view that should be made visible below the _Entry_ and/or _Dropdown_|

## Enhanced Entry
### Basic XAML Usage
```
TO BE ADDED
```
| Property | Type | Default | Description |
|---|---|---|---|
|**ShowKeyboardOnFocus**|Boolean|True|Will allow/disallow the keyboard from showing on focus of the entry field.|
|**SelectAllOnFocus**|Boolean|False|Will select all text on focus of the entry field|


## Enhanced Button
### Basic XAML Usage
```
TO BE ADDED
```

| Property | Type | Default | Description |
|---|---|---|---|
|**Padding**|Thickness|Platform specific|Will change the internal padding of a button to allow for modification of the height/width of the button to make a minimalist style button|

## TreeView
### Basic XAML Usage
```
<?xml version="1.0" encoding="utf-8" ?>
<ContentPage xmlns="http://xamarin.com/schemas/2014/forms"
             xmlns:x="http://schemas.microsoft.com/winfx/2009/xaml"
             xmlns:controls="clr-namespace:XFControls.Controls"
             x:Class="XFControls.MainPage">
    <ContentPage.Resources>
        <ResourceDictionary>
            <Style x:Key="BaseListViewStyle" TargetType="controls:TreeView">
                <Setter Property="Margin" Value="0" />
                <Setter Property="Padding" Value="0" />
                <Setter Property="VerticalOptions" Value="FillAndExpand" />
            </Style>
        </ResourceDictionary>
    </ContentPage.Resources>

    <ContentPage.Content>
        <controls:TreeView ItemsSource="{Binding TreeNodes}" LeafItemSelectedCommand="{Binding TreeViewItemSelectedCommand}"
                           Title="Header Text" HeaderTextColor="White" HeaderBackgroundColor="#006072"
                           Style="{StaticResource BaseListViewStyle}" />
    </ContentPage.Content>
</ContentPage>

```
| Property | Type | Default | Description |
|---|---|---|---|
|**ItemsSource**|System.Collections.IEnumerable|null|The datasource for the dropdown. Should use the included **XFCTreeviewNodeViewModel**|
|**LeafItemSelectedCommand**|ICommand|null|The command to be executed when a leaf has been clicked.|
|**NodeContent**|DataTemplate|TreeViewNode|The template to use within each of the nodes|
|**ShowExpandAll**|bool|true|Determines where an *Expand All* button is made available|
|**ShowCollapseAll**|bool|true|Determines where a *Collapse All* button is made available|
|**Title**|string|string.Empty|The text of the title to display in the header. Also determines if a header is displayed|
|**HeaderBackgroundColor**|Color|Xamarin.FormsColor.Gray|The background color of the Header|
|**HeaderTextColor**|Color|Xamarin.Forms.Color.Black|The text color fo the Header|