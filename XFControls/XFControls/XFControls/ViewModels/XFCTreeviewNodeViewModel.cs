﻿using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using XFControls.Controls;
using XFControls.Helper;

namespace XFControls.ViewModels
{
    public class XFCTreeViewNodeViewModel : XFCOnPropertyChanged
    {
        #region Private Variables

        private bool _isExpanded = false;
        private bool _isSelected = false;
        private Color _currentBackgroundColor = Color.Transparent;

        #endregion

        #region Constructor

        public XFCTreeViewNodeViewModel(int id, string title, int level = 0, string subtitle = "", object tag = null, IEnumerable<XFCTreeViewNodeViewModel> nodes = null)
        {
            ID = id;
            Title = title;
            Level = level;
            Tag = tag;
            Nodes = nodes;
        }

        #endregion

        #region Public Properties

        public int ID { get; set; }

        public string Title { get; set; }

        public int Level { get; set; }

        public string Subtitle { get; set; }

        public object Tag { get; set; }

        public IEnumerable<XFCTreeViewNodeViewModel> Nodes { get; set; }

        #endregion

        #region UI Properties

        public Color CurrentBackgroundColor
        {
            get => _currentBackgroundColor;
            set => SetValue(ref _currentBackgroundColor, value);
        }

        public bool IsLeaf => Nodes == null || Nodes.Count() == 0;

        public bool IsLeafSelected => IsLeaf && IsSelected;

        public bool IsExpanded
        {
            get => _isExpanded;
            set
            {
                SetValue(ref _isExpanded, value);
                OnPropertyChanged(nameof(CurrentIcon));
            }
        }

        public bool IsSelected
        {
            get => _isSelected;
            set => SetValue(ref _isSelected, value);
        }

        public string CurrentIcon => XFCTreeViewNodeHelper.GetIcon(IsLeaf, IsExpanded);

        public Thickness Indent => XFCTreeViewNodeHelper.GetIndent(Level);

        public bool ShowLeafIcon
        {
            get
            {
                if (IsLeaf)
                    return XFCTreeViewNode.ShowLeafIcon;

                return true;
            }
        }

        #endregion
    }
}
