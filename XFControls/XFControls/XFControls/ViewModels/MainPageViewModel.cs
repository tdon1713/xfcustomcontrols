﻿using Acr.UserDialogs;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using XFControls.Helper;

namespace XFControls.ViewModels
{
    public class MainPageViewModel : XFCOnPropertyChanged
    {
        #region Private Variables

        private ICommand _treeViewItemSelectedCommand;

        private ObservableCollection<XFCDropdownItemViewModel> _dropdownItems;
        private XFCDropdownItemViewModel _selectedDropdownItem;

        private ObservableCollection<XFCTreeViewNodeViewModel> _treeNodes;

        #endregion

        #region Public Properties

        public ICommand TreeViewItemSelectedCommand => _treeViewItemSelectedCommand ?? (_treeViewItemSelectedCommand = new Command<XFCTreeViewNodeViewModel>(async (selected) => await UI_ItemSelected(selected)));

        public ObservableCollection<XFCTreeViewNodeViewModel> TreeNodes
        {
            get => _treeNodes;
            set => SetValue(ref _treeNodes, value);
        }

        public ObservableCollection<XFCDropdownItemViewModel> DropdownItems
        {
            get => _dropdownItems;
            set => SetValue(ref _dropdownItems, value);
        }

        public XFCDropdownItemViewModel SelectedDropdownItem
        {
            get => _selectedDropdownItem;
            set => SetValue(ref _selectedDropdownItem, value);
        }

        #endregion

        #region Constructor

        public MainPageViewModel()
        {
            Task.Run(() =>
            {
                DropdownItems = new ObservableCollection<XFCDropdownItemViewModel>()
                {
                    new XFCDropdownItemViewModel(1, "Test 1"),
                    new XFCDropdownItemViewModel(2, "Test 2"),
                    new XFCDropdownItemViewModel(3, "Test 3"),
                    new XFCDropdownItemViewModel(4, "Test 4"),
                    new XFCDropdownItemViewModel(5, "Test 5")
                };

                XFCTreeViewNode.Init();
                XFCTreeViewNode.SetLeafStyle(new XFCTreeViewNodeLeafStyle
                {
                    LeafSelectedColor = Color.FromHex("7fafb8")
                });

                TreeNodes = new ObservableCollection<XFCTreeViewNodeViewModel>()
                {
                    new XFCTreeViewNodeViewModel(1, "Test 1-1", 0, nodes: new List<XFCTreeViewNodeViewModel> {
                        new XFCTreeViewNodeViewModel(2, "Test 1.2-1", 1, nodes: new List<XFCTreeViewNodeViewModel> {
                            new XFCTreeViewNodeViewModel(3, "Test 1.2.3-1", 2)
                        }),
                    }),
                    new XFCTreeViewNodeViewModel(4, "Test 1-2", 0, nodes: new List<XFCTreeViewNodeViewModel> {
                        new XFCTreeViewNodeViewModel(5, "Test 1.2-1", 1),
                        new XFCTreeViewNodeViewModel(6, "Test 1.2-2", 1, nodes: new List<XFCTreeViewNodeViewModel> {
                            new XFCTreeViewNodeViewModel(7, "Test 1.2.3-1", 2, nodes: new List<XFCTreeViewNodeViewModel> {
                                new XFCTreeViewNodeViewModel(8, "Test 1.2.3.4-1", 3)
                            }),
                            new XFCTreeViewNodeViewModel(9, "Test 1.2.3-2", 2)
                        }),
                        new XFCTreeViewNodeViewModel(10, "Test 1.2-3", 1, nodes: new List<XFCTreeViewNodeViewModel> {
                            new XFCTreeViewNodeViewModel(11, "Test 1.2.3-1", 2),
                            new XFCTreeViewNodeViewModel(12, "Test 1.2.3-2", 2)
                        })
                    }),
                    new XFCTreeViewNodeViewModel(13, "Test 1-3", 0),
                    new XFCTreeViewNodeViewModel(14, "Test 1-4", 0)
                };
            });
        }


        #endregion

        #region Private Regions

        private async Task UI_ItemSelected(XFCTreeViewNodeViewModel node)
        {
            await UserDialogs.Instance.AlertAsync(node.Title);
        }

        #endregion
    }
}
