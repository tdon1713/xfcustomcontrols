﻿namespace XFControls.ViewModels
{
    public class XFCDropdownItemViewModel
    {
        #region Constructor

        public XFCDropdownItemViewModel() { }

        public XFCDropdownItemViewModel(int id, string value, object tag = null)
        {
            ID = id;
            Value = value;
            Tag = tag;
        }

        #endregion

        #region Public Properties

        public int ID { get; set; }

        public string Value { get; set; }

        public object Tag { get; set; }

        #endregion
    }
}
