﻿using Xamarin.Forms;
using XFControls.ViewModels;

namespace XFControls
{
    public partial class MainPage : ContentPage
    {
        #region Public Properties

        public MainPageViewModel ViewModel
        {
            get => BindingContext as MainPageViewModel;
            set => BindingContext = value;
        }

        #endregion

        #region Constructor

        public MainPage()
        {
            InitializeComponent();
            ViewModel = new MainPageViewModel();
        }

        #endregion
    }
}
