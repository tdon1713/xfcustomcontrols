﻿using Xamarin.Forms;
using XFControls.Controls;

namespace XFControls.Helper
{
    public static class XFCTreeViewNodeHelper
    {
        public static Thickness GetIndent(int level)
        {
            int leftMargin = 0;
            if (level != 0)
                leftMargin = XFCTreeViewNode.LeftMarginMultiplier * level;

            return new Thickness(leftMargin + 5, 0, 5, 0);
        }

        public static string GetIcon(bool isLeaf, bool isExpanded)
        {
            if (isLeaf)
                return FontAwesomeFontValues.CheckSquare;

            if (isExpanded)
                return XFCTreeViewNode.ExpandedIcon;

            return XFCTreeViewNode.CollapsedIcon;
        }
    }
}
