﻿using Xamarin.Forms;
using XFControls.Controls;

namespace XFControls.Helper
{
    public static class XFCTreeViewNode
    {
        private static XFCTreeViewNodeStyle _nodeStyle;
        private static XFCTreeViewNodeLeafStyle _leafStyle;

        public static void Init()
        {
            _nodeStyle = new XFCTreeViewNodeStyle();
            _leafStyle = new XFCTreeViewNodeLeafStyle();
        }

        public static void SetIcons(XFCTreeViewNodeStyle nodeStyle)
        {
            _nodeStyle = nodeStyle ?? new XFCTreeViewNodeStyle();
        }

        public static void SetLeafStyle(XFCTreeViewNodeLeafStyle leafStyle)
        {
            _leafStyle = leafStyle ?? new XFCTreeViewNodeLeafStyle();
        }

        public static string CollapsedIcon => _nodeStyle.CollapsedIcon;

        public static string ExpandedIcon => _nodeStyle.ExpandedIcon;

        public static bool ShowLeafIcon => _leafStyle.ShowLeafIcon;

        public static Color LeafSelectedColor => _leafStyle.LeafSelectedColor;

        public static int LeftMarginMultiplier => _nodeStyle.LeftMarginMultiplier;
    }
}
