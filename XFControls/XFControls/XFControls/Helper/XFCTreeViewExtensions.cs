﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using XFControls.ViewModels;

namespace XFControls.Helper
{
    public static class XFCTreeViewExtensions
    {
        public static IEnumerable Add(this IEnumerable source, XFCTreeViewNodeViewModel selectedItem)
        {
            foreach (var current in source)
            {
                yield return current;

                if (selectedItem.ID == (current as XFCTreeViewNodeViewModel).ID)
                {
                    foreach (var newCurrent in selectedItem.Nodes)
                    {
                        yield return newCurrent;
                    }
                }
            }
        }

        public static IEnumerable CollapseAll(this IEnumerable source)
        {
            return new ObservableCollection<XFCTreeViewNodeViewModel>(source.Cast<XFCTreeViewNodeViewModel>()
                .Select(item =>
                {
                    item.IsExpanded = false;
                    item.IsSelected = false;
                    return item;
                }).Where(item => item.Level == 0));
        }

        public static IEnumerable ExpandAll(this IEnumerable source)
        {
            List<XFCTreeViewNodeViewModel> nodes = new List<XFCTreeViewNodeViewModel>();
            foreach (XFCTreeViewNodeViewModel item in source)
            {
                item.IsExpanded = true;
                if (!nodes.Contains(item)) 
                    nodes.Add(item);
                
                GetItemsForExpandAll(item.Nodes, nodes);
            }

            return new ObservableCollection<XFCTreeViewNodeViewModel>(nodes);
        }

        public static IEnumerable Remove(this IEnumerable source, XFCTreeViewNodeViewModel selectedItem)
        {
            List<XFCTreeViewNodeViewModel> itemsToRemove = new List<XFCTreeViewNodeViewModel>();
            GetItemsToRemove(selectedItem, itemsToRemove);

            List<XFCTreeViewNodeViewModel> newSource = new List<XFCTreeViewNodeViewModel>();
            foreach (XFCTreeViewNodeViewModel current in source)
            {
                if (current.ID == selectedItem.ID)
                {
                    current.IsExpanded = false;
                }

                if (itemsToRemove.Contains(current))
                {
                    current.IsExpanded = false;
                    continue;
                }

                newSource.Add(current);
            }

            return newSource;
        }

        private static void GetItemsToRemove(XFCTreeViewNodeViewModel selectedItem, List<XFCTreeViewNodeViewModel> list)
        {
            if (selectedItem.IsLeaf)
            {
                list.Add(selectedItem);
                return;
            }

            foreach (var node in selectedItem.Nodes)
            {
                GetItemsToRemove(node, list);
                list.Add(node);
            }
        }

        private static void GetItemsForExpandAll(IEnumerable source, List<XFCTreeViewNodeViewModel> list)
        {
            if (source == null)
            {
                return;
            }

            foreach (XFCTreeViewNodeViewModel item in source)
            {
                item.IsExpanded = true;
                if (!list.Contains(item))
                    list.Add(item);

                GetItemsForExpandAll(item.Nodes, list);
            }
        }
    }
}
