﻿using System.Drawing;

namespace XFControls.Helper
{
    public class XFCTreeViewNodeLeafStyle
    {
        public XFCTreeViewNodeLeafStyle() { }

        public bool ShowLeafIcon { get; set; } = false;

        public Color LeafSelectedColor { get; set; } = Color.Transparent;
    }
}
