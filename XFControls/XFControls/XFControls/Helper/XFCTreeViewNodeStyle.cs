﻿using XFControls.Controls;

namespace XFControls.Helper
{
    public class XFCTreeViewNodeStyle
    {
        public XFCTreeViewNodeStyle() { }

        public string CollapsedIcon { get; private set; } = FontAwesomeFontValues.PlusSquare;

        public string ExpandedIcon { get; private set; } = FontAwesomeFontValues.MinusSquare;

        public int LeftMarginMultiplier { get; set; } = 15;
    }
}
