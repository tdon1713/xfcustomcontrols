﻿namespace XFControls.Interfaces
{
    public interface IKeyboardHelper 
    {
        bool IsShowing();

        void ToggleKeyboard();
    }
}
