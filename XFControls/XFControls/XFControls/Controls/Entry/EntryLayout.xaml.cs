﻿using XFControls.Interfaces;
using XFControls.ViewModels;
using System;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Windows.Input;
using XFControls.Helper;

namespace XFControls.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EntryLayout : ContentView
    {
        #region Frame

        public static readonly BindableProperty ShowFrameShadowProperty = BindableProperty.Create(
            nameof(ShowFrameShadow),
            typeof(bool),
            typeof(EntryLayout),
            defaultValue: false,
            defaultBindingMode: BindingMode.TwoWay,
            propertyChanged: (sender, oldValue, newValue) =>
            {
                var control = (EntryLayout)sender;
                if ((bool)newValue)
                {
                    control.EntryLayoutContainer.HasShadow = true;
                    if (!control.ShowFrameBorder)
                    {
                        control.EntryLayoutContainer.BorderColor = Color.White;
                    }
                }
                else
                {
                    control.EntryLayoutContainer.HasShadow = false;
                }
            });

        public bool ShowFrameShadow
        {
            get => (bool)GetValue(ShowFrameShadowProperty);
            set => SetValue(ShowFrameShadowProperty, value);
        }

        public static readonly BindableProperty ShowFrameBorderProperty = BindableProperty.Create(
            nameof(ShowFrameBorder),
            typeof(bool),
            typeof(EntryLayout),
            defaultValue: false,
            defaultBindingMode: BindingMode.TwoWay,
            propertyChanged: (sender, oldValue, newValue) =>
            {
                var control = (EntryLayout)sender;
                if ((bool)newValue)
                {
                    control.EntryLayoutContainer.BorderColor = Color.Black;
                }
                else
                {
                    control.EntryLayoutContainer.BorderColor = Color.Transparent;
                }
            });

        public bool ShowFrameBorder
        {
            get => (bool)GetValue(ShowFrameBorderProperty);
            set => SetValue(ShowFrameBorderProperty, value);
        }

        #endregion

        #region Header Label

        public static readonly BindableProperty HeaderTextProperty = BindableProperty.Create(
            nameof(HeaderText),
            typeof(string),
            typeof(EntryLayout),
            defaultValue: string.Empty,
            defaultBindingMode: BindingMode.TwoWay,
            propertyChanged: (sender, oldValue, newValue) =>
            {
                var control = (EntryLayout)sender;
                control.Header.Text = (string)newValue;
                control.Header.IsVisible = !String.IsNullOrWhiteSpace((string)newValue);
            });

        public string HeaderText
        {
            get => (string)GetValue(HeaderTextProperty);
            set => SetValue(HeaderTextProperty, value);
        }

        public static readonly BindableProperty HeaderPositionProperty = BindableProperty.Create(
            nameof(HeaderPosition),
            typeof(XFCEntryLayoutHeaderPosition),
            typeof(EntryLayout),
            defaultValue: XFCEntryLayoutHeaderPosition.Top,
            propertyChanged: (sender, oldValue, newValue) =>
            {
                var control = (EntryLayout)sender;
                switch ((XFCEntryLayoutHeaderPosition)newValue)
                {
                    case XFCEntryLayoutHeaderPosition.Top:
                        control.Header.IsVisible = true;
                        control.Header.Margin = new Thickness(5, 3, 0, 0);
                        control.Header.VerticalTextAlignment = TextAlignment.End;
                        control.Header.HorizontalOptions = LayoutOptions.FillAndExpand;
                        break;
                    case XFCEntryLayoutHeaderPosition.Left:
                        control.StackContainer.Orientation = StackOrientation.Horizontal;
                        control.Header.IsVisible = true;
                        control.Header.Margin = new Thickness(5, 5, 0, 0);
                        control.Header.VerticalTextAlignment = TextAlignment.Center;
                        control.Header.HorizontalOptions = LayoutOptions.Start;
                        break;
                    case XFCEntryLayoutHeaderPosition.Placeholder:
                        control.Header.IsVisible = false;
                        control.Header.Margin = new Thickness(0);
                        control.ValueEntry.Placeholder = control.HeaderText;
                        break;
                }
            });

        public XFCEntryLayoutHeaderPosition HeaderPosition
        {
            get => (XFCEntryLayoutHeaderPosition)GetValue(HeaderPositionProperty);
            set => SetValue(HeaderPositionProperty, value);
        }
        #endregion

        #region Entry

        public static BindableProperty EntryStyleProperty = BindableProperty.Create(
            nameof(EntryStyle),
            typeof(Style),
            typeof(EntryLayout),
            defaultValue: null,
            propertyChanged: (sender, oldValue, newValue) =>
            {
                var control = (EntryLayout)sender;
                if (newValue != null)
                {
                    control.ValueEntry.Style = (Style)newValue;
                }
                else
                {
                    control.ValueEntry.Style = null;
                }
            });

        public Style EntryStyle
        {
            get => (Style)GetValue(EntryStyleProperty);
            set => SetValue(EntryStyleProperty, value);
        }

        public static BindableProperty EntryTextProperty = BindableProperty.Create(
            nameof(EntryText),
            typeof(string),
            typeof(EntryLayout),
            defaultValue: string.Empty,
            defaultBindingMode: BindingMode.TwoWay,
            propertyChanged: (sender, oldValue, newValue) =>
            {
                ((EntryLayout)sender).TextChanged(newValue?.ToString() ?? string.Empty);
            });

        public string EntryText
        {
            get => (string)GetValue(EntryTextProperty);
            set => SetValue(EntryTextProperty, value);
        }

        public void TextChanged(string newText)
        {
            Device.BeginInvokeOnMainThread(() => ValueEntry.Text = newText);
        }

        public static readonly BindableProperty EntryCompletedCommandProperty = BindableProperty.Create(
            nameof(EntryCompletedCommand),
            typeof(ICommand),
            typeof(EntryLayout),
            defaultValue: null);

        public ICommand EntryCompletedCommand
        {
            get => (ICommand)GetValue(EntryCompletedCommandProperty);
            set => SetValue(EntryCompletedCommandProperty, value);
        }

        #endregion

        #region Search Button

        public static readonly BindableProperty SearchCommandProperty = BindableProperty.Create(
            nameof(SearchCommand),
            typeof(ICommand),
            typeof(EntryLayout),
            defaultValue: null,
            propertyChanged: (sender, oldValue, newValue) =>
            {
                var control = (EntryLayout)sender;
                if (newValue != null)
                {
                    control.SearchButton.IsVisible = true;
                }
                else
                {
                    control.SearchButton.IsVisible = false;
                }
            });

        public ICommand SearchCommand
        {
            get => (ICommand)GetValue(SearchCommandProperty);
            set => SetValue(SearchCommandProperty, value);
        }

        #endregion

        #region Keyboard Button

        public static readonly BindableProperty ShowKeyboardButtonProperty = BindableProperty.Create(
            nameof(ShowKeyboardButton),
            typeof(bool),
            typeof(EntryLayout),
            defaultValue: false,
            defaultBindingMode: BindingMode.TwoWay,
            propertyChanged: (sender, oldValue, newValue) =>
            {
                var control = (EntryLayout)sender;
                control.KeyboardButton.IsVisible = (bool)newValue;
            });

        public bool ShowKeyboardButton
        {
            get => (bool)GetValue(ShowKeyboardButtonProperty);
            set => SetValue(ShowKeyboardButtonProperty, value);
        }

        #endregion

        #region Add Button

        public static readonly BindableProperty AddCommandProperty = BindableProperty.Create(
            nameof(AddCommand),
            typeof(ICommand),
            typeof(EntryLayout),
            defaultValue: null,
            propertyChanged: (sender, oldValue, newValue) =>
            {
                var control = (EntryLayout)sender;
                control.AddButton.IsVisible = newValue != null;
            });

        public ICommand AddCommand
        {
            get => (ICommand)GetValue(AddCommandProperty);
            set => SetValue(AddCommandProperty, value);
        }

        public static readonly BindableProperty AddCommandParameterProperty = BindableProperty.Create(
            nameof(AddCommandParameter),
            typeof(object),
            typeof(EntryLayout),
            defaultValue: null);

        public object AddCommandParameter
        {
            get => GetValue(AddCommandParameterProperty);
            set => SetValue(AddCommandParameterProperty, value);
        }

        #endregion

        #region Dropdown List

        public static readonly BindableProperty ItemsSourceProperty = BindableProperty.Create(
            nameof(ItemsSource),
            typeof(System.Collections.IEnumerable),
            typeof(EntryLayout),
            null,
            propertyChanged: (sender, oldValue, newValue) =>
            {
                var control = (EntryLayout)sender;
                var items = (System.Collections.IEnumerable)newValue;
                control.ListItems.ItemsSource = items;
                control.ListItems.ItemTapped -= control.ListItems_ItemTapped;

                if (items != null)
                {
                    control.DropdownButton.IsVisible = true;
                    control.ValueEntry.IsEnabled = control.AllowManualEntry;
                    control.ListItems.ItemTapped += control.ListItems_ItemTapped;
                }
                else
                {
                    control.DropdownButton.IsVisible = false;
                    control.ValueEntry.IsEnabled = true;
                }
            });

        public System.Collections.IEnumerable ItemsSource
        {
            get => (System.Collections.IEnumerable)GetValue(ItemsSourceProperty);
            set => SetValue(ItemsSourceProperty, value);
        }

        public static readonly BindableProperty DropdownSelectedItemProperty = BindableProperty.Create(
            nameof(DropdownSelectedItem),
            typeof(object),
            typeof(EntryLayout),
            defaultValue: null,
            defaultBindingMode: BindingMode.TwoWay);

        public object DropdownSelectedItem
        {
            get => GetValue(DropdownSelectedItemProperty);
            set => SetValue(DropdownSelectedItemProperty, value);
        }

        public static readonly BindableProperty AllowManualEntryProperty = BindableProperty.Create(
            nameof(AllowManualEntry),
            typeof(bool),
            typeof(EntryLayout),
            defaultValue: true,
            propertyChanged: (sender, oldValue, newValue) =>
            {
                var control = (EntryLayout)sender;
                if (control.ItemsSource != null)
                {
                    control.ValueEntry.IsEnabled = (bool)newValue;
                }
                else
                {
                    control.ValueEntry.IsEnabled = true;
                }
            });

        public bool AllowManualEntry
        {
            get => (bool)GetValue(AllowManualEntryProperty);
            set => SetValue(AllowManualEntryProperty, value);
        }

        #endregion

        #region Internal View

        public static readonly BindableProperty LayoutContentProperty = BindableProperty.Create(
            nameof(LayoutContent),
            typeof(View),
            typeof(EntryLayout),
            defaultValue: null,
            propertyChanged: (sender, oldValue, newValue) =>
            {
                var control = (EntryLayout)sender;
                if (newValue == null)
                {
                    control.LayoutContentContainer.Content = null;
                    return;
                }

                control.LayoutContentContainer.Content = (View)newValue;
            });


        public View LayoutContent
        {
            get => (View)GetValue(ContentProperty);
            set => SetValue(ContentProperty, value);
        }

        #endregion

        #region Constructor

        public EntryLayout()
        {
            InitializeComponent();
        }

        #endregion

        #region Events
        
        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
            if (ListItems.BindingContext == null)
            {
                ListItems.ItemTapped -= ListItems_ItemTapped;
            }
        }

        private void KeyboardButton_Clicked(object sender, EventArgs e)
        {
            var keyboardService = DependencyService.Get<IKeyboardHelper>();
            keyboardService.ToggleKeyboard();
            ValueEntry.Focus();
        }

        private void DropdownButton_Clicked(object sender, EventArgs e)
        {
            ListItems.IsVisible = !ListItems.IsVisible;
        }

        private void SearchButton_Clicked(object sender, EventArgs e)
        {
            if (SearchCommand != null && SearchCommand.CanExecute(null))
            {
                SearchCommand.Execute(null);
            }
        }

        private void AddButton_Clicked(object sender, EventArgs e)
        {
            if (AddCommand != null && AddCommand.CanExecute(null))
            {
                AddCommand.Execute(null);
            }
        }

        private void ListItems_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item != null)
            {
                ListItems.IsVisible = false;
                ListItems.SelectedItem = null;

                DropdownSelectedItem = e.Item;
            }
        }

        private void ValueEntry_Completed(object sender, EventArgs e)
        {
            EntryText = ValueEntry.Text;

            if (!String.IsNullOrWhiteSpace(ValueEntry.Text))
            {
                if (this.ItemsSource != null)
                {
                    DropdownSelectedItem = new XFCDropdownItemViewModel
                    {
                        ID = -1,
                        Value = ValueEntry.Text
                    };

                    ListItems.IsVisible = false;
                }
            }

            if (this.EntryCompletedCommand != null && this.EntryCompletedCommand.CanExecute(ValueEntry.Text))
            {
                EntryCompletedCommand.Execute(ValueEntry.Text);
            }
        }

        #endregion
    }
}