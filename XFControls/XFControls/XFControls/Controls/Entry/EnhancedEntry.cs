﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XFControls.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public class EnhancedEntry : Entry
    {
        #region Hide SIP

        public static readonly BindableProperty ShowKeyboardOnFocusProperty = BindableProperty.Create(
            nameof(ShowKeyboardOnFocus),
            typeof(bool),
            typeof(EnhancedEntry),
            true);

        public bool ShowKeyboardOnFocus
        {
            get => (bool)GetValue(ShowKeyboardOnFocusProperty);
            set => SetValue(ShowKeyboardOnFocusProperty, value);
        }

        #endregion

        #region Select All

        public static readonly BindableProperty SelectAllOnFocusProperty = BindableProperty.Create(
            nameof(SelectAllOnFocus),
            typeof(bool),
            typeof(EnhancedEntry),
            false);

        public bool SelectAllOnFocus
        {
            get => (bool)GetValue(SelectAllOnFocusProperty);
            set => SetValue(SelectAllOnFocusProperty, value);
        }

        #endregion
    }
}
