﻿using System;
using System.Collections;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XFControls.Helper;
using XFControls.ViewModels;

namespace XFControls.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TreeView : ContentView
    {
        #region ItemsSource

        public static BindableProperty ItemsSourceProperty = BindableProperty.Create(
            nameof(ItemsSource),
            typeof(IEnumerable),
            typeof(TreeView),
            defaultValue: null,
            propertyChanged: (sender, oldValue, newValue) =>
            {
                var control = (TreeView)sender;
                control.tree.ItemsSource = (IEnumerable)newValue;
            });

        public IEnumerable ItemsSource
        {
            get => (IEnumerable)GetValue(ItemsSourceProperty);
            set => SetValue(ItemsSourceProperty, value);
        }

        #endregion

        #region Item Selected Command

        public static BindableProperty LeafItemSelectedCommandProperty = BindableProperty.Create(
            nameof(LeafItemSelectedCommand),
            typeof(ICommand),
            typeof(TreeView),
            defaultValue: null);

        public ICommand LeafItemSelectedCommand
        {
            get { return (ICommand)GetValue(LeafItemSelectedCommandProperty); }
            set { SetValue(LeafItemSelectedCommandProperty, value); }
        }

        #endregion

        #region Node

        public static readonly BindableProperty NodeContentProperty = BindableProperty.Create(
            nameof(NodeContent),
            typeof(DataTemplate),
            typeof(TreeView),
            defaultValue: default(TreeViewNode),
            propertyChanged: (sender, oldValue, newValue) =>
            {
                var control = (TreeView)sender;
                control.tree.ItemTemplate = (DataTemplate)newValue;
            });

        public DataTemplate NodeContent
        {
            get => (DataTemplate)GetValue(NodeContentProperty);
            set => SetValue(NodeContentProperty, value);
        }

        #endregion

        #region Expand All

        public static BindableProperty ShowExpandAllProperty = BindableProperty.Create(
            nameof(ShowExpandAll),
            typeof(bool),
            typeof(TreeView),
            defaultValue: false,
            propertyChanged: (sender, oldValue, newValue) =>
            {
                var control = (TreeView)sender;
                control.ExpandAllButton.IsVisible = (bool)newValue;
            });

        public bool ShowExpandAll
        {
            get => (bool)GetValue(ShowExpandAllProperty);
            set => SetValue(ShowExpandAllProperty, value);
        }

        #endregion

        #region Collapse All

        public static BindableProperty ShowCollapseAllProperty = BindableProperty.Create(
            nameof(ShowCollapseAll),
            typeof(bool),
            typeof(TreeView),
            defaultValue: false,
            propertyChanged: (sender, oldValue, newValue) =>
            {
                var control = (TreeView)sender;
                control.CollapseAllButton.IsVisible = (bool)newValue;
            });

        public bool ShowCollapseAll
        {
            get => (bool)GetValue(ShowCollapseAllProperty);
            set => SetValue(ShowCollapseAllProperty, value);
        }

        #endregion

        #region Header

        public static BindableProperty TitleProperty = BindableProperty.Create(
            nameof(Title),
            typeof(string),
            typeof(TreeView),
            defaultValue: string.Empty, 
            propertyChanged: (sender, oldValue, newValue) =>
            {
                var control = (TreeView)sender;
                string header = (string)newValue;
                control.HeaderText.Text = header;
                control.HeaderContainer.IsVisible = !String.IsNullOrWhiteSpace(header);
            });

        public string Title
        {
            get => (string)GetValue(TitleProperty);
            set => SetValue(TitleProperty, value);
        }

        public static BindableProperty HeaderBackgroundColorProperty = BindableProperty.Create(
            nameof(HeaderBackgroundColor),
            typeof(Color),
            typeof(TreeView),
            defaultValue: Color.Gray,
            propertyChanged: (sender, oldValue, newValue) =>
            {
                var control = (TreeView)sender;
                control.HeaderContainer.BackgroundColor = (Color)newValue;
            });

        public Color HeaderBackgroundColor
        {
            get => (Color)GetValue(HeaderBackgroundColorProperty);
            set => SetValue(HeaderBackgroundColorProperty, value);
        }

        public static BindableProperty HeaderTextColorProperty = BindableProperty.Create(
            nameof(HeaderTextColor),
            typeof(Color),
            typeof(TreeView),
            defaultValue: Color.Black,
            propertyChanged: (sender, oldValue, newValue) =>
            {
                var control = (TreeView)sender;
                control.HeaderText.TextColor = (Color)newValue;
            });

        public Color HeaderTextColor
        {
            get => (Color)GetValue(HeaderTextColorProperty);
            set => SetValue(HeaderTextColorProperty, value);
        }

        #endregion

        public TreeView()
        {
            InitializeComponent();
        }

        #region Events

        void tree_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null)
            {
                return;
            }

            var item = (e.Item as XFCTreeViewNodeViewModel);
            foreach (XFCTreeViewNodeViewModel source in ItemsSource)
            {
                source.IsSelected = false;
                source.CurrentBackgroundColor = Color.Transparent;
            }

            item.IsSelected = !item.IsSelected;

            if (!item.IsLeaf)
            {
                if (item.IsExpanded)
                {
                    ItemsSource = ItemsSource.Remove(item);
                }
                else
                {
                    ItemsSource = ItemsSource.Add(item);
                    item.IsExpanded = !item.IsExpanded;
                }
            }
            else 
            {
                if (item.IsSelected)
                    item.CurrentBackgroundColor = XFCTreeViewNode.LeafSelectedColor;

                if (LeafItemSelectedCommand != null && LeafItemSelectedCommand.CanExecute(item))
                    LeafItemSelectedCommand.Execute(e.Item);
            }

            tree.SelectedItem = null;
        }

        private void ExpandAllButton_Clicked(object sender, System.EventArgs e)
        {
            ItemsSource = ItemsSource.ExpandAll();
        }

        private void CollapseAllButton_Clicked(object sender, System.EventArgs e)
        {
            ItemsSource = ItemsSource.CollapseAll();
        }

        #endregion
    }
}