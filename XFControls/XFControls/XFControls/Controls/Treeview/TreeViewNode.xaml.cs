﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XFControls.Controls
{
    public static class FontAwesomeFontValues
    {
        public static readonly string PlusSquare = "\uf0fe";
        public static readonly string MinusSquare = "\uf146";
        public static readonly string CaretRight = "\uf0da";
        public static readonly string CaretDown = "\uf0d7";
        public static readonly string CheckSquare = "\uf14a";
            }

    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TreeViewNode : DataTemplate
    {
        public TreeViewNode ()
		{
			InitializeComponent ();
		}
	}
}