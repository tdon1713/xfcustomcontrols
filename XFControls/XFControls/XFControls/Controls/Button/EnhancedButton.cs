﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XFControls.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public class EnhancedButton : Button
    {
        public static readonly BindableProperty PaddingProperty = BindableProperty.Create(
            nameof(Padding),
            typeof(Thickness),
            typeof(EnhancedButton),
            defaultValue: new Thickness(-1));

        public Thickness Padding
        {
            get => (Thickness)GetValue(PaddingProperty);
            set => SetValue(PaddingProperty, value);
        }
    }
}