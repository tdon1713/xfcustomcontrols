﻿
using Android.App;
using Android.Content;
using Android.Views.InputMethods;
using XFControls.Interfaces;
using Xamarin.Forms;

[assembly: Dependency(typeof(XFControls.Droid.Services.KeyboardHelper))]

namespace XFControls.Droid.Services
{
    public class KeyboardHelper : IKeyboardHelper
    {
        public bool IsShowing()
        {
            var context = Forms.Context;
            var inputMethodManager = context.GetSystemService(Context.InputMethodService) as InputMethodManager;
            return inputMethodManager?.IsAcceptingText ?? false;
        }

        public void ToggleKeyboard()
        {
            var context = Forms.Context;
            var inputMethodManager = context.GetSystemService(Context.InputMethodService) as InputMethodManager;
            if (inputMethodManager != null)
            {
                var activity = context as Activity;
                var token = activity.CurrentFocus?.WindowToken;
                inputMethodManager.ToggleSoftInput(ShowFlags.Forced, HideSoftInputFlags.None);
            }
        }
    }
}