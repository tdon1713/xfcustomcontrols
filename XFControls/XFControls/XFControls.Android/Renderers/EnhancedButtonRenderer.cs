﻿using XFControls.Droid;
using XFControls.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(EnhancedButton), typeof(EnhancedButtonRenderer))]

namespace XFControls.Droid
{
#pragma warning disable CS0618 // Type or member is obsolete
    public class EnhancedButtonRenderer : ButtonRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
        {
            base.OnElementChanged(e);

            var nativeElement = Element as EnhancedButton;
            if (nativeElement != null)
            {
                //TODO: Need to look at how we do this in eztrac.
                if (nativeElement.Padding != null && nativeElement.Padding != new Thickness(-1))
                {
                    this.Control.SetPadding(
                        (int)nativeElement.Padding.Left,
                        (int)nativeElement.Padding.Top,
                        (int)nativeElement.Padding.Right,
                        (int)nativeElement.Padding.Bottom);
                }

                this.Control.SetIncludeFontPadding(false);
            }
        }
    }
#pragma warning restore CS0618 // Type or member is obsolete
}