﻿using XFControls.Controls;
using XFControls.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(EnhancedEntry), typeof(EnhancedEntryRenderer))]

namespace XFControls.Droid
{
#pragma warning disable CS0618 // Type or member is obsolete
    public class EnhancedEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement == null)
            {
                var nativeElement = Element as EnhancedEntry;
                if (nativeElement != null)
                {
                    if (!nativeElement.ShowKeyboardOnFocus)
                    {
                        Control.ShowSoftInputOnFocus = false;
                    }

                    if (nativeElement.SelectAllOnFocus)
                    {
                        var nativeEditText = (global::Android.Widget.EditText)Control;
                        nativeEditText.SetSelectAllOnFocus(true);

                        Control.FocusChange += (sender, eh) =>
                        {
                            if (eh.HasFocus)
                            {
                                nativeEditText.SelectAll();
                            }
                        };
                    }
                }
            }
        }
    }
#pragma warning restore CS0618 // Type or member is obsolete
}